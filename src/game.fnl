(var t 0)
(var scale 1)
(var bounce 0)

(fn fill [x y to-c]
 "Flood fill a colour"
 (let [on-c (pix x y)]
   (when (not= on-c to-c)
    (fn flood [x y]
     (when (= (pix x y) on-c)
      (pix x y to-c)
      (when (> x 0)
       (flood (- x 1) y))
      (when (< x 239)
       (flood (+ x 1) y))
      (when (> y 0)
       (flood x (- y 1)))
      (when (< y 135)
       (flood x (+ y 1)))))
    (flood x y))))

 (fn scale-point [p s]
  (let [(px py s) (values p.x p.y s)
   sx (* px s)
   sy (* py s)]
   {:x sx :y sy}))

(fn _G.TIC []
 (cls 10)
 (local x 120)
 (local y 68)
 (var origin {:x x :y y})
 (set scale (+ scale (/ (sin bounce) 12)))

 (for [i 1 40]
  (for [j 1 40]
   (local x (* 16 (^ (sin t) 3)))
   (local y (- (- (* 13 (cos t)) 
                  (* 5 (cos (* 2 t))) 
                  (* 2 (cos (* 3 t))) 
                  (cos (* 4 t)))))
   (local sp (scale-point {:x x :y y} scale))
   (pix (+ sp.x origin.x) (+ sp.y origin.y) 2)
   (set t (+ t 1))))
   
  (for [i 1 40]
  (for [j 1 40]
   (local x (* 16 (^ (sin t) 3)))
   (local y (- (- (* 13 (cos t)) 
                  (* 5 (cos (* 2 t))) 
                  (* 2 (cos (* 3 t))) 
                  (cos (* 4 t)))))
   (local sp (scale-point {:x x :y y} (- scale 0.05)))
   (pix (+ sp.x origin.x) (+ sp.y origin.y) 12)
   (set t (+ t 1))))
   
 (fill x y 2)  
 
 (set bounce (+ bounce 0.1)))

