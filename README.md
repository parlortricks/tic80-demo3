# demo3

## Summary
demo3 demonstrates drawing the heart curve

## License
[SPDX-License-Identifier: GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2021 parlortricks