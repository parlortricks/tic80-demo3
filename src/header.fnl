;; title:   demo3
;; author:  parlortricks
;; desc:    demo3 demonstrates drawing the heart curve
;; website: http://tic80.com/dev?id=6589
;; script:  fennel
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright (c) 2021 parlortricks

(local screen-width 240)
(local screen-height 136)
(local sin math.sin)
(local cos math.cos)
(local atan2 math.atan2)
(local pi math.pi)
